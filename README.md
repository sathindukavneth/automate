# Ballerina Hackathon 2017

AutoMate is an mobile based application which facilitates the connectivity of different stakeholders in the transportation industry.  It will allow the vehicle users to identify vehicle repair stations and service centers closest to their locations which will be useful in case of an emergency. The app will allow the users to find out the exact services offered as well discover user ratings and costs involved at each service center. The application will also connect other emergency service providers such as insurance services and hospitals on to the platform allowing safer and better transport system

Currently we are retrieving the details of the nearest service providers based on the province. And we have connected the Ballerina backend with Uber APIs to get Uber Cars aroud the location. 

# Future developments for our project

   * Include an exact location with google places API and retrieve location based data
   * Include advanced support with uber API
   * Include machine learning to the app
    
# Issues faced

   * Lack of documentation
   * Build command not working
   * Doesn't allow unused inputs
   * Comments not allowed in imports (before main method)
   * Issue with MySQL connection closing when returning value
   * [Problem with WSO2 cloud] connection strings are having an issue with mysql even when the correct credentials are given
   
# Pros of the language

  * Concept is powerfull and very good
  * Provided documentation style is nice (explaining each step)
  * Provided UI is nice
  * Easy to use and readable syntax
  
# Things we liked
  * Composer tool
  
# Things we hated
  * Lack of support with other platform interigations
  * Documentation insufficient and even blanks in the API
  
