package hackathon;

import ballerina.net.http;
import ballerina.data.sql;
import ballerina.lang.messages;

@http:configuration{basePath:"/automate"}
service<http> ServiceCenters {
    
    map props = {"jdbcUrl":"jdbc:mysql://localhost:3306/baldb","username":"sathindukavneth", "password":"password"};
    sql:ClientConnector balDB;
    sql:Parameter[] params = [];

    @http:GET{}
    @http:Path {value:"/GetServices"}
    resource GetCenters (message m, @http:QueryParam{value:"location"} string location) {
         
        balDB = create sql:ClientConnector(props);
        
        datatable dt = sql:ClientConnector.select(balDB, "SELECT * FROM providers WHERE province = '" + location + "'", params);
        var jsonRes, err = <json>dt;
        
        message response = {};
        messages:setJsonPayload(response, jsonRes);
        
        sql:ClientConnector.close(balDB);
        
        reply response;

    }
    

}