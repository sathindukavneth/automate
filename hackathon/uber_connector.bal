import ballerina.net.http;
import ballerina.lang.messages;

@http:configuration {basePath:"/uber"}
service<http> headerBasedRouting {
  @http:GET {}
    @http:Path {value:"/estimates"}
    resource hbrResource (message m) {
        http:ClientConnector weatherEP = create http:ClientConnector("https://api.uber.com");
    message response = {};
    message newMessage = {};
    messages:setHeader(newMessage,"Authorization","Token token");
    response = http:ClientConnector.execute(weatherEP, "GET","/v1.2/estimates/time?start_latitude=37.7752315&start_longitude=-122.418075",newMessage);
    reply response;
  }
}
