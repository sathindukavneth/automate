package com.qsolutions.projectezyblood.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.qsolutions.projectezyblood.MainActivity;
import com.qsolutions.projectezyblood.R;
import com.qsolutions.projectezyblood.adapters.ExpandableListAdapter;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by Choxmi on 2015-12-30.
 */

public class FragmentDonateBlood extends Fragment{

    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_donateblood,container,false);

        ImageView submit = (ImageView)rootView.findViewById(R.id.image_request_requestblood);
        final EditText recepientName = (EditText)rootView.findViewById(R.id.input_request_name);
        final EditText recepientEmail = (EditText)rootView.findViewById(R.id.input_request_email);
        final EditText recepientMobile = (EditText)rootView.findViewById(R.id.input_request_mobile);
        final EditText recepientMessage = (EditText)rootView.findViewById(R.id.input_request_message);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recepientName!=null && recepientMobile!=null){
                    try {
                        ParseObject donations = new ParseObject("Donations");
                        donations.put("User", ((MainActivity) getActivity()).getEmail());
                        donations.put("RecepientName", recepientName.getText().toString());
                        donations.put("RecepientEmail", recepientEmail.getText().toString());
                        donations.put("RecepientMobile", recepientMobile.getText().toString());
                        if (recepientMessage != null) {
                            donations.put("Message", recepientMessage.getText().toString());
                        }
                        donations.saveEventually();
                        Toast.makeText(getActivity(), "Donation Submitted", Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        Log.e("Error",e.toString());
                    }
                }else {
                    Toast.makeText(getActivity(),"Complete Textboxes",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }
}
