package com.qsolutions.projectezyblood.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.qsolutions.projectezyblood.R;

/**
 * Created by Choxmi on 10/14/2016.
 */
public class FragmentServiceSelector extends android.support.v4.app.Fragment {

    ImageView ninety,tire,garage,tow;
    android.support.v4.app.Fragment fragment = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_service_selector, container, false);

        ninety = (ImageView)rootView.findViewById(R.id.ninety);
        tire = (ImageView)rootView.findViewById(R.id.tire);

        ninety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new FragmentGasStation();
                android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
            }
        });

        return rootView;
    }
}
