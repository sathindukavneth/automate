package com.qsolutions.projectezyblood.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.qsolutions.projectezyblood.R;

/**
 * Created by Sathindu on 2015-12-30.
 */
public class FragmentAbout extends Fragment {

    ImageView button_about_email;
    ImageView button_about_fbook;

    public FragmentAbout(){

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_about, container, false);

        button_about_email = (ImageView) rootView.findViewById(R.id.button_about_email);
        button_about_email.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");

                try{
                    Log.d("Package" , intent.getPackage());
                    Toast.makeText(getActivity(),"Past Package " + intent.getPackage() ,Toast.LENGTH_LONG).show();
                    intent.setPackage("com.google.android.gm");
                    Toast.makeText(getActivity(), "New Package " + intent.getPackage(), Toast.LENGTH_LONG).show();
                }
                catch (Exception e){
                    try {
                        intent.setPackage("com.google.android.apps.inbox");
                    }catch (Exception ex){
                        Toast.makeText(getActivity(),"Inbox have not been installed.",Toast.LENGTH_LONG).show();
                    }

                }

                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "qsolutionsnetwork@gmail.com" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "Regarding EZYBlood");
                intent.putExtra(Intent.EXTRA_TEXT, "Message : ");
                startActivity(Intent.createChooser(intent, ""));
            }
        });

        button_about_fbook = (ImageView) rootView.findViewById(R.id.button_about_fb);
        button_about_fbook.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.facebook.com/qsolutionsnetwork/"));
                startActivity(intent);
            }
        });

        return rootView;
    }
}
