package com.qsolutions.projectezyblood.fragments;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.qsolutions.projectezyblood.Location.GPSTracker;
import com.qsolutions.projectezyblood.R;
import com.qsolutions.projectezyblood.adapters.CustomAdapter;

import java.util.List;

/**
 * Created by Sathindu on 2016-01-21.
 */

public class FragmentSkipRequestBlood extends Fragment implements OnMapReadyCallback {

    private GoogleMap googleMap;
    int spinner_position;

    double latitude = 6.9344;
    double longtitude = 79.8428;

    EditText request_name;
    EditText request_email;
    EditText request_mobile;
    EditText request_message;
    Spinner request_bloodgroup;
    TextView request_arealable;
    Spinner request_area;
    ImageView request_blood_image;

    ProgressDialog mDialog;

    public FragmentSkipRequestBlood() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_skip_requestblood, container, false);

        MapFragment mapFragment = (MapFragment) this.getChildFragmentManager().findFragmentById(R.id.s_map_request_location);
        mapFragment.getMapAsync(this);

        request_name = (EditText) rootView.findViewById(R.id.s_input_request_name);
        request_email = (EditText) rootView.findViewById(R.id.s_input_request_email);
        request_mobile = (EditText) rootView.findViewById(R.id.s_input_request_mobile);
        request_message = (EditText) rootView.findViewById(R.id.s_input_request_message);
        request_bloodgroup = (Spinner) rootView.findViewById(R.id.s_spinner_request_bloodtype);
        request_arealable = (TextView) rootView.findViewById(R.id.s_text_request_area);
        request_area = (Spinner) rootView.findViewById(R.id.s_spinner_request_area);
        request_blood_image = (ImageView) rootView.findViewById(R.id.s_image_request_requestblood);



        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Processing...");
        mDialog.setCancelable(false);

        final String[] blood_groups_list = new String[]{"Blood Group", "A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"};
        final String[] area_list = new String[]{"Area Range", "10", "20", "30"};
        int hiddenItem = 0;

        request_blood_image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                makeRequest();

            }
        });

        CustomAdapter adapter = new CustomAdapter(getActivity(), android.R.layout.simple_spinner_item, blood_groups_list, hiddenItem);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        request_bloodgroup.setAdapter(adapter);
        spinner_position = adapter.getPosition("Blood Group");
        request_bloodgroup.setSelection(spinner_position);
        request_bloodgroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 6 || position == 8) {
                    request_arealable.setVisibility(View.VISIBLE);
                    request_area.setVisibility(View.VISIBLE);
                } else {
                    request_arealable.setVisibility(View.GONE);
                    request_area.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        CustomAdapter adapterArea = new CustomAdapter(getActivity(), android.R.layout.simple_spinner_item, area_list, hiddenItem);
        adapterArea.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        request_area.setAdapter(adapterArea);
        spinner_position = adapterArea.getPosition("Area Range");
        request_area.setSelection(spinner_position);

        request_name.requestFocus();

        return rootView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMyLocationEnabled(true);

        setCurrentLocation();
        manualSetMap();

        //code to set latitude longitude of current location when press location button in map
        this.googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                setCurrentLocation();
                manualSetMap();
                return false;
            }
        });
    }

    private void setCurrentLocation() {
//        try {
//            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//            boolean status = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//            if (status == true) {
//                Location location = this.googleMap.getMyLocation();
//
//                latitude = location.getLatitude();
//                longtitude = location.getLongitude();
//
//            } else {
//                Toast.makeText(getActivity(), "Enable Location", Toast.LENGTH_SHORT).show();
//            }
//        } catch (Exception e) {
//            Log.d("SET CURRENT LOCATION",e.toString());
//        }

        GPSTracker gps = new GPSTracker(getActivity());
        if(gps.canGetLocation()){
            latitude = gps.getLatitude();
            longtitude = gps.getLongitude();
            Toast.makeText(getActivity(), "Location Set to Your Current Location", Toast.LENGTH_SHORT).show();
        }else{
            gps.showSettingsAlert();
        }
        //gps.stopUsingGPS();
    }

    public void manualSetMap() {
        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longtitude)).title("Your Saved Location");
        Log.e("MAP SET", longtitude + "/" + latitude);

        this.googleMap.clear();
        this.googleMap.addMarker(marker);
        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longtitude), 15);
        this.googleMap.animateCamera(update);
    }

    private void makeRequest() {
        if(checkFields()){
            mDialog.show();

            String blood = getBloodType();
            Double range = 10.0;

            String area_range = request_area.getItemAtPosition(request_area.getSelectedItemPosition()).toString();
            if(area_range == "Area Range"){
                range = 10.0;
            }else{
                range = Double.parseDouble(area_range);
            }

            Log.d("===> AREA RANGE <===",area_range);

            ParseGeoPoint point = new ParseGeoPoint(latitude, longtitude);

            ParseObject Request = new ParseObject("BloodRequest");

            Request.put("name", request_name.getText().toString());
            Request.put("email", request_email.getText().toString());
            Request.put("mobile", request_mobile.getText().toString());
            Request.put("message", request_message.getText().toString());
            Request.put("bloodtype", blood);
            Request.put("range", range);
            Request.put("location", point);

            sendNotification(blood, point, range, Request);
        }
    }

    private String getBloodType() {
        return request_bloodgroup.getItemAtPosition(request_bloodgroup.getSelectedItemPosition()).toString();
    }

    public void sendNotification(String blood, ParseGeoPoint currentLocation, double range, final ParseObject request) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.whereEqualTo("bloodgroup", blood);
        query.whereWithinKilometers("location", currentLocation, range);

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> profile, ParseException e) {
                if (e == null) {
                    mDialog.hide();
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Request Confirmation")
                            .setMessage("Number of Matching Donors - "+profile.size())
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Log.d("Load Profile", "NO ERROR");
                                    mDialog.show();
                                    request.saveEventually();
                                    mDialog.hide();
                                    Toast.makeText(getActivity(), "Request Completed", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();

                } else {
                    mDialog.hide();
                    Toast.makeText(getActivity(), "Error in Connecting", Toast.LENGTH_SHORT).show();
                    Log.d("Request Blood", "ERROR");
                }
            }
        });
    }


    private boolean checkFields(){
        if(request_name.getText().toString().trim().length() == 0){
            Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
            return false;
        }else if(request_email.getText().toString().trim().length() == 0){
            Toast.makeText(getActivity(), "Enter Email", Toast.LENGTH_SHORT).show();
            return false;
        }else if(request_mobile.getText().toString().trim().length() == 0){
            Toast.makeText(getActivity(), "Enter Phone Number", Toast.LENGTH_SHORT).show();
            return false;
        }else if(getBloodType() == "Blood Group"){
            Toast.makeText(getActivity(), "Select Blood Group", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }
}



