package com.qsolutions.projectezyblood;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Choxmi on 10/15/2016.
 */
public class Locations {
    private String store;
    private double lat;
    private double lng;
    private String address;
    private String type;
    private int tel;

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }
}
