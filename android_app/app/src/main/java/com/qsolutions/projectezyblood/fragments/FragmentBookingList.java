package com.qsolutions.projectezyblood.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.qsolutions.projectezyblood.BookingModel;
import com.qsolutions.projectezyblood.HttpHandler;
import com.qsolutions.projectezyblood.Locations;
import com.qsolutions.projectezyblood.R;
import com.qsolutions.projectezyblood.adapters.BookingListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Choxmi on 10/16/2016.
 */

public class FragmentBookingList extends Fragment {

    RecyclerView recyclerView;
    private BookingListAdapter bookingListAdapter;
    List<BookingModel> model = new ArrayList<>();
    ArrayList<Locations> loc = new ArrayList<>();
    ProgressDialog progressDialog;
    String url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.booking_list_viewer, container, false);
        progressDialog = new ProgressDialog(getContext());
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rowBooking);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String area = sharedPref.getString("area","colombo");
        String service = sharedPref.getString("service","Fuel");

        String type = service.replaceAll(" ","-");
        url = "http://10.0.2.2:8080/WebServiceRest/webresources/webService/"+area+"&-and-type=&"+type+"&";
        new GetDetails().execute();
        bookingListAdapter = new BookingListAdapter(getContext(),model);
        recyclerView.setAdapter(bookingListAdapter);
        return rootView;
    }

    private class GetDetails extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog= new ProgressDialog(getContext());
            progressDialog.setMessage("Retreiveing data...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpHandler httpHandler = new HttpHandler();

            JSONArray jsonStr = httpHandler.makeServiceCall(url);
            Log.e("Response","Response is "+jsonStr);
            for(int i=0;i<jsonStr.length();i++){
                try {

                    JSONObject obj = jsonStr.getJSONObject(i);

                    Locations locations = new Locations();
                    locations.setStore(obj.getString("name"));
                    locations.setLat(obj.getDouble("latitude"));
                    locations.setLng(obj.getDouble("longtitude"));
                    locations.setAddress(obj.getString("address"));
                    locations.setType(obj.getString("type"));
                    locations.setTel(obj.getInt("tel"));
                    Log.e("Latitude",String.valueOf(locations.getLat()));
                    Log.e("Longtitude",String.valueOf(locations.getLng()));
                    loc.add(locations);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog.isShowing())
                progressDialog.dismiss();

            for(int j=0;j<loc.size();j++){
                BookingModel bookingModel = new BookingModel();
                bookingModel.setTxt1(loc.get(j).getStore());
                bookingModel.setTxt2(loc.get(j).getAddress()+"\n"+loc.get(j).getTel());
                model.add(bookingModel);
                bookingListAdapter = new BookingListAdapter(getContext(),model);
                recyclerView.setAdapter(bookingListAdapter);
            }
            }
    }
}
