package com.qsolutions.projectezyblood.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import com.qsolutions.projectezyblood.Location.GPSTracker;
import com.qsolutions.projectezyblood.MainActivity;
import com.qsolutions.projectezyblood.R;

/**
 * Created by Sathindu on 2016-01-20.
 */

public class FragmentProfile extends android.support.v4.app.Fragment implements OnMapReadyCallback {

    EditText profile_email;
    EditText profile_name;
    RadioGroup profile_gender;
    RadioButton profile_male;
    RadioButton profile_female;
    Spinner profile_blood;
    TextView profile_birthdate;

    ProgressDialog mDialog;

    private GoogleMap googleMap;
    double latitude = 6.9344;
    double longtitude = 79.8428;

    String email;

    public FragmentProfile() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map_profile_fragment);
        mapFragment.getMapAsync(this);

        ImageView create = (ImageView) rootView.findViewById(R.id.createBtn);
        create.setImageResource(R.drawable.createprofile);

        ImageView cancel = (ImageView) rootView.findViewById(R.id.cancelBtn);
        cancel.setImageResource(R.drawable.cancelcreate);

        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Processing...");
        mDialog.setCancelable(false);

        profile_email = (EditText) rootView.findViewById(R.id.input_profile_email);
        profile_name = (EditText) rootView.findViewById(R.id.input_profile_name);
        profile_gender = (RadioGroup) rootView.findViewById(R.id.gender_group);
        profile_male = (RadioButton) rootView.findViewById(R.id.radio_profile_male);
        profile_female = (RadioButton) rootView.findViewById(R.id.radio_profile_female);
        profile_blood = (Spinner) rootView.findViewById(R.id.spinner_profile_blood);
        profile_birthdate = (TextView) rootView.findViewById(R.id.text_profile_birthdate);

        email = ((MainActivity)getActivity()).getEmail();

        create.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                    if(checkFields()){
                        mDialog.show();
                        createProfile();
                    }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loadProfile();
            }
        });


        profile_birthdate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                android.support.v4.app.DialogFragment newFragment = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        String string_month;
                        String string_day;

                        if(month < 9){
                            string_month = "0"+(month+1);
                        }else{
                            string_month = Integer.toString(month + 1);
                        }
                        if(day < 10){
                            string_day = "0"+Integer.toString(day);
                        }else{
                            string_day = Integer.toString(day);
                        }

                        profile_birthdate.setText(string_day + "/" + string_month  + "/" + year);
                    }
                };
                newFragment.show(getChildFragmentManager(), "datePicker");
            }
        });

        profile_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Male Selected", Toast.LENGTH_SHORT).show();
            }
        });

        profile_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Female Selected", Toast.LENGTH_SHORT).show();
            }
        });

        mDialog.show();
        loadProfile();

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        manualSetMap();

        //code to set latitude longitude of current location when press location button in map
        this.googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                setCurrentLocation();
                return false;
            }
        });
    }

    public void manualSetMap(){
        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longtitude)).title("Your Saved Location");
        Log.e("MAP SET", longtitude + "/" + latitude);

        this.googleMap.clear();
        this.googleMap.addMarker(marker);
        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longtitude), 15);
        this.googleMap.animateCamera(update);
    }

    private void createProfile() {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.whereEqualTo("email", email);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> profile, ParseException e) {
                if (e == null) {
                    Log.d("Parse Get Query", "NO EXCEPTION");

                    if (profile.isEmpty()) {
                        Log.d("Parse Get Query", "EMPTY");

                        //Set Location on Google Map
                        setCurrentLocation();
                        ParseGeoPoint point = new ParseGeoPoint(latitude, longtitude);

                        ParseObject newProfile = new ParseObject("UserProfile");

                        newProfile.put("email", email);
                        newProfile.put("name", profile_name.getText().toString());
                        newProfile.put("gender", getGender());
                        newProfile.put("bloodgroup", getBloodType());
                        newProfile.put("birthdate", profile_birthdate.getText());
                        newProfile.put("location", point);
                        newProfile.saveEventually();

                        mDialog.hide();
                        Toast.makeText(getActivity(), "Created Successfully", Toast.LENGTH_SHORT).show();
                        MainActivity m = new MainActivity();
                        m.setTitle();
                        android.support.v4.app.Fragment redirect = new FragmentGasStation();
                        android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, redirect).commit();
                    } else {
                        Log.d("Parse Get Query", "NOT EMPTY" + profile.size());

                        //Set location on the Google Map
                        ParseGeoPoint point = new ParseGeoPoint(latitude, longtitude);

                        profile.get(0).put("name", profile_name.getText().toString());
                        profile.get(0).put("gender", getGender());
                        profile.get(0).put("bloodgroup", getBloodType());
                        profile.get(0).put("birthdate", profile_birthdate.getText().toString());
                        profile.get(0).put("location", point);
                        profile.get(0).saveEventually();

                        mDialog.hide();
                        Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                        MainActivity m = new MainActivity();
                        m.setTitle();
                        android.support.v4.app.Fragment redirect = new FragmentGasStation();
                        android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, redirect).commit();
                    }

                } else {
                    mDialog.hide();
                    Log.d("score", "Error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Error in Connecting to Server", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void loadProfile() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProfile");
        query.whereEqualTo("email", email);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> profile, ParseException e) {
                if (e == null) {
                    Log.d("Load Profile", "NO ERROR");

                    if (profile.isEmpty()) {
                        profile_email.setText(email);
                        mDialog.hide();
                        Log.d("Load Profile", "Empty Profile");
                        Toast.makeText(getActivity(), "No Profile Found, Please Update.", Toast.LENGTH_SHORT).show();

                    } else {
                        profile_email.setText(email);
                        profile_name.setText(profile.get(0).getString("name"));
                        profile_birthdate.setText(profile.get(0).getString("birthdate"));
                        setGender(profile.get(0).getString("gender"));
                        setBloodGroup(profile.get(0).getString("bloodgroup"));

                        ParseGeoPoint p = profile.get(0).getParseGeoPoint("location");
                        latitude = p.getLatitude();
                        longtitude = p.getLongitude();
                        manualSetMap();

                        mDialog.hide();
                    }

                } else {
                    mDialog.hide();
                    Toast.makeText(getActivity(), "Error in Loading", Toast.LENGTH_SHORT).show();
                    Log.d("Load Profile", "ERROR");
                }
            }
        });
    }

    //method to set Longitude and Latitude to current location
    private void setCurrentLocation(){
//        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//        boolean status = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        if (status == true){
//            Location location = this.googleMap.getMyLocation();
//
//            latitude = location.getLatitude();
//            longtitude = location.getLongitude();
//            manualSetMap();
//        }else{
//            Toast.makeText(getActivity(), "Enable Location", Toast.LENGTH_SHORT).show();
//        }

        GPSTracker gps = new GPSTracker(getActivity());
        if(gps.canGetLocation()){
            latitude = gps.getLatitude();
            longtitude = gps.getLongitude();
            Toast.makeText(getActivity(), "Location Set to Your Current Location", Toast.LENGTH_SHORT).show();
        }else{
            gps.showSettingsAlert();
        }

    }

    private String getGender(){
        if(profile_male.isChecked()){
            return "Male";
        }else{
            return "Female";
        }
    }

    private String getBloodType(){
         return profile_blood.getItemAtPosition(profile_blood.getSelectedItemPosition()).toString();
    }

    private void setBloodGroup(String blood){
        String compareValue = blood;
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.bloodgroups, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        profile_blood.setAdapter(adapter);
        if (!compareValue.equals(null)) {
            int spinnerPosition = adapter.getPosition(compareValue);
            profile_blood.setSelection(spinnerPosition);
        }
    }

    private void setGender(String gender){
        if(gender.equals("Male")){
            profile_male.setChecked(true);
        }else{
            profile_female.setChecked(true);
        }
    }

    private boolean checkFields(){
        if(profile_email.getText().toString().trim().length() == 0){
            Toast.makeText(getActivity(), "Enter Email", Toast.LENGTH_SHORT).show();
            return false;
        }else if(profile_name.getText().toString().trim().length() == 0){
            Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_SHORT).show();
            return false;
        }else if(profile_birthdate.getText().toString().trim().equals("Tap to set Birthdate...") ){
            Toast.makeText(getActivity(), "Enter Birthdate", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }
}

