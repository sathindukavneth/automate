package com.qsolutions.projectezyblood.login;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.qsolutions.projectezyblood.R;

/**
 * Created by Choxmi on 1/20/2016.
 */
public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean emailSent = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState
        );
        setContentView(R.layout.activity_forgot_password);
    }

    @Override
    public void onClick(View v) {
        if(checkNetworkState()) {
            if(!emailSent) {
                ParseUser.requestPasswordResetInBackground("gwcsathsara@gmail.com",
                        new RequestPasswordResetCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    Toast.makeText(ForgotPasswordActivity.this, "Email Sent", Toast.LENGTH_SHORT).show();
                                    emailSent = true;
                                } else {
                                    if (e.getCode() == ParseException.INVALID_EMAIL_ADDRESS ||
                                            e.getCode() == ParseException.EMAIL_NOT_FOUND) {
                                        Toast.makeText(ForgotPasswordActivity.this, R.string.com_parse_ui_invalid_email_toast, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(ForgotPasswordActivity.this, R.string.com_parse_ui_login_help_submit_failed_unknown, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
            }else{
                Toast.makeText(ForgotPasswordActivity.this, "Email Already Sent", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(ForgotPasswordActivity.this, "Check connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkNetworkState(){
        ConnectivityManager connec = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connec != null && (
                (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) ||
                        (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED))) {

            return true;

        } else if (connec != null && (
                (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) ||
                        (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED ))) {
            Toast.makeText(getApplicationContext(), "You must be connected to the internet", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }
}
