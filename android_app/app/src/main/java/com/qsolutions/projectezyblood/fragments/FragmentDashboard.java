package com.qsolutions.projectezyblood.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.qsolutions.projectezyblood.MainActivity;
import com.qsolutions.projectezyblood.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Choxmi on 4/3/2016.
 */
public class FragmentDashboard extends Fragment {

    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_dashboard,container,false);

        try {
            String email = ((MainActivity) getActivity()).getEmail();
            ParseQuery<ParseObject> donation = ParseQuery.getQuery("Donations");
            donation.whereEqualTo("User", email);
        }catch (Exception e){
            Toast.makeText(getActivity(),e.toString(),Toast.LENGTH_SHORT).show();
            Log.e("Error",e.toString());
        }
        return rootView;
    }


}
