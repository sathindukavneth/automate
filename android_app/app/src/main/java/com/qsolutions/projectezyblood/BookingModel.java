package com.qsolutions.projectezyblood;

import android.graphics.Bitmap;

/**
 * Created by Choxmi on 10/16/2016.
 */
public class BookingModel {
    private String txt1;
    private String txt2;

    public String getTxt1() {
        return txt1;
    }

    public void setTxt1(String txt1) {
        this.txt1 = txt1;
    }

    public String getTxt2() {
        return txt2;
    }

    public void setTxt2(String txt2) {
        this.txt2 = txt2;
    }
}
