package com.qsolutions.projectezyblood.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import com.parse.ParseObject;
import com.qsolutions.projectezyblood.MainActivity;
import com.qsolutions.projectezyblood.R;

/**
 * Created by Choxmi on 2016-01-06.
 */
public class FragmentSettings extends Fragment {

    ProgressDialog mDialog;

    public FragmentSettings(){

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        mDialog = new ProgressDialog(getActivity());
        final String[] settingsList = new String[]{"Deactivate Account","Report"};

        final ArrayList<String> settings = new ArrayList<String>();
        for(int i = 0;i<settingsList.length;++i){
            settings.add(settingsList[i]);
        }

        final ListView settingView = (ListView) rootView.findViewById(R.id._list_settings_actions);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,settingsList);
        settingView.setAdapter(adapter);

        settingView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object selectedSetting = settingView.getItemAtPosition(position);
                String selectedName = (String) selectedSetting;

                if (selectedName == "Deactivate Account") {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert
                            .setTitle("Confirm deactivation")
                            .setMessage("Are you sure you want to deactivate your account?")
                            .setIcon(R.drawable.deactivate)
                            .setPositiveButton("Deactivate", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(), "Successfully Deactivated", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(), "Successfully Cancelled", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .show();
                }

                if (selectedName == "Report") {
                    final View dialogView = inflater.inflate(R.layout.report_setting, null);
                    new AlertDialog.Builder(getActivity())
                            .setView(dialogView)
                            .setPositiveButton("Report", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        final EditText text = (EditText) dialogView.findViewById(R.id.report_txt);
                                        final Spinner spin = (Spinner) dialogView.findViewById(R.id.report_spinner);
                                        mDialog.show();
                                        ParseObject reportObject = new ParseObject("Report");
                                        reportObject.put("User", ((MainActivity) getActivity()).getEmail());
                                        reportObject.put("Type", spin.getSelectedItem().toString());
                                        reportObject.put("Description", text.getText().toString());
                                        reportObject.saveEventually();
                                        Toast.makeText(getActivity(), "Submit Successful", Toast.LENGTH_SHORT).show();
                                        mDialog.hide();
                                    } catch (Exception e) {
                                        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .show();
                }

            }
        });

        return rootView;
    }
}
