package com.qsolutions.projectezyblood.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qsolutions.projectezyblood.BookingModel;
import com.qsolutions.projectezyblood.R;

import java.util.List;

/**
 * Created by Choxmi on 10/16/2016.
 */
public class BookingListAdapter extends RecyclerView.Adapter<BookingListAdapter.StudentViewHolder>{
    List<BookingModel> students;
    Context context;
    LayoutInflater layoutInflater;

    public BookingListAdapter(Context context,List<BookingModel> students){
        this.context = context;
        this.students = students;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.fragment_booking_list,parent,false);
        return new StudentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StudentViewHolder holder, int position) {
        BookingModel stu = students.get(position);
        holder.text1.setText(stu.getTxt1());
        holder.text2.setText(stu.getTxt2());
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public class StudentViewHolder extends RecyclerView.ViewHolder{
        ImageView profile;
        TextView text1,text2;
        public StudentViewHolder(View itemView) {
            super(itemView);
            profile = (ImageView)itemView.findViewById(R.id.booking_img);
            text1 = (TextView)itemView.findViewById(R.id.booking_head);
            text2 = (TextView)itemView.findViewById(R.id.booking_content);
        }
    }
}

