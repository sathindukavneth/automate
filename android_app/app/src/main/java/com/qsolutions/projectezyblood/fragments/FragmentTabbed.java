package com.qsolutions.projectezyblood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.qsolutions.projectezyblood.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Choxmi on 10/15/2016.
 */
public class FragmentTabbed extends Fragment {
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    private FragmentTabHost tabHost;

    public FragmentTabbed() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        tabHost = new FragmentTabHost(getActivity());
        tabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_tabbed);

        Bundle arg1 = new Bundle();
        arg1.putInt("Arg for Frag1", 1);
        tabHost.addTab((tabHost.newTabSpec("Tab1").setIndicator("Tab1")),
                FragmentGasStation.class, arg1);

        Bundle arg2 = new Bundle();
        arg2.putInt("Arg for Frag2", 2);
        tabHost.addTab((tabHost.newTabSpec("Tab2").setIndicator("Tab 2")),
                FragmentTyre.class, arg2);


        return tabHost;
    }
}
