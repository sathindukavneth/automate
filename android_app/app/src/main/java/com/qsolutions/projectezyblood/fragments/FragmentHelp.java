package com.qsolutions.projectezyblood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qsolutions.projectezyblood.R;

/**
 * Created by Sathindu on 2015-12-30.
 */
public class FragmentHelp extends Fragment {

    public FragmentHelp(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_help, container, false);

        return rootView;
    }
}
