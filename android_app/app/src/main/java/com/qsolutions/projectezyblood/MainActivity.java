package com.qsolutions.projectezyblood;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.qsolutions.projectezyblood.fragments.FragmentAbout;
import com.qsolutions.projectezyblood.fragments.FragmentBooking;
import com.qsolutions.projectezyblood.fragments.FragmentDashboard;
import com.qsolutions.projectezyblood.fragments.FragmentDonateBlood;
import com.qsolutions.projectezyblood.fragments.FragmentHelp;
import com.qsolutions.projectezyblood.fragments.FragmentNotifications;
import com.qsolutions.projectezyblood.fragments.FragmentGasStation;
import com.qsolutions.projectezyblood.fragments.FragmentServiceSelector;
import com.qsolutions.projectezyblood.fragments.FragmentSettings;
import com.qsolutions.projectezyblood.fragments.FragmentTabbed;
import com.qsolutions.projectezyblood.login.ActivityBuilder;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String email;
    private String name;
    private String objectId;
    TextView emailView;
    TextView userView;

    public String getEmail() {
        return email;
    }

    public String getName() {

        return name;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View header = LayoutInflater.from(this).inflate(R.layout.nav_header_main, null);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        emailView = (TextView) header.findViewById(R.id.navigationHeaderEmail);
        userView = (TextView) header.findViewById(R.id.navigationName);
        Log.e("Awe", getEmail() + getName());
        navigationView.addHeaderView(header);
        email = getIntent().getExtras().getString("Email");
        name = getIntent().getExtras().getString("Name");
        objectId = getIntent().getExtras().getString("Obs");

        emailView.setText(getEmail());
        userView.setText(getName());

        ParseInstallation p = ParseInstallation.getCurrentInstallation();
        Log.e("Installation", p.toString());
        p.saveInBackground();

        Log.e("Email",email);
        if(email.equals("(Logged in with facebook)")){
            Log.e("Inside","Thread");
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("FacebookUsers");
                    query.whereEqualTo("UserName", objectId);
                    Log.e("Query", "Checking the query");
                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, com.parse.ParseException e) {
                            email = objects.get(0).get("Email").toString();
                            name = objects.get(0).get("Name").toString();
                            emailView.setText(getEmail());
                            userView.setText(getName());
                        }
                    });
                }
            });

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setTitle("AutoMate");
        android.support.v4.app.Fragment initialfragment = new FragmentDashboard();
        if (initialfragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, initialfragment).commit();
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            moveTaskToBack(true);
                        }
                    }).create().show();
        }
    }

    public void setTitle(){
        getSupportActionBar().setTitle("AutoMate");
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        android.support.v4.app.Fragment fragment = null;

        if (id == R.id.nav_profile) {
            getSupportActionBar().setTitle("Emergency Services");
            fragment = new FragmentServiceSelector();
        } else if (id == R.id.nav_requestBlood) {
            getSupportActionBar().setTitle("Bookings");
            fragment = new FragmentBooking();
        }  else if (id == R.id.nav_logout) {
            logout();
        } else if (id == R.id.nav_settings) {
            getSupportActionBar().setTitle("Settings");
            fragment = new FragmentSettings();
        } else if (id == R.id.nav_help) {
            getSupportActionBar().setTitle("Help");
            fragment = new FragmentHelp();
        } else if (id == R.id.nav_about) {
            getSupportActionBar().setTitle("About Us");
            fragment = new FragmentAbout();
        }else if (id == R.id.nav_dashboard) {
            getSupportActionBar().setTitle("Dashboard");
            fragment = new FragmentDashboard();
        }


        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logout() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert
                .setTitle("Log Out")
                .setMessage("Are you sure you want to Log Out?")
                .setIcon(R.drawable.logout)
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Successfully Logged Out", Toast.LENGTH_SHORT).show();
                        ParseUser.logOut();
                        ActivityBuilder loginBuilder = new ActivityBuilder(MainActivity.this, LoginListener.class);
                        startActivityForResult(loginBuilder.build(), 0);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Successfully Cancelled", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }
}
