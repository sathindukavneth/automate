package com.qsolutions.projectezyblood.login;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ui.ParseLoginConfig;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;

import java.util.List;

/**
 * Created by Choxmi on 1/15/2016.
 */
public class ActivityBuilder {

    private Context context;
    private Class className;
    public String email;
    public String userName;
    private ParseLoginConfig config = new ParseLoginConfig();
    private ParseUser sendUser;




    public ActivityBuilder(Context context,Class className) {
        this.context = context;
        this.className = className;
    }

    public ActivityBuilder(Context context,Class className,ParseUser user) {
        this.sendUser = user;
        if(user.getEmail()!=null) {
            this.email = user.getEmail();
            this.userName = user.get("name").toString();
        }
        else{
            this.email = "(Logged in with facebook)";
            this.userName = "Facebook User";
        }
        this.context = context;
        this.className = className;
    }

    public Intent build() {
        Intent intent = new Intent(context, className);
        intent.putExtras(config.toBundle());
        return intent;
    }

    public Intent mainbuild() {
        Intent intent = new Intent(context, className);
        intent.putExtra("Email", email);
        intent.putExtra("Name", userName);
        intent.putExtra("Obs", sendUser.getUsername());
        intent.putExtras(config.toBundle());
        return intent;
    }
}
