package com.qsolutions.projectezyblood.fragments;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.gms.maps.SupportMapFragment;
import com.qsolutions.projectezyblood.Locations;
import com.qsolutions.projectezyblood.R;
import com.qsolutions.projectezyblood.TimePickerFragment;

import java.util.ArrayList;

/**
 * Created by Choxmi on 10/16/2016.
 */
public class FragmentBooking extends Fragment {

    Button checkButton;
    ImageButton dateBtn,timeBtn;
    Spinner servicetype,locationcombo;

    public FragmentBooking(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bookings, container, false);
        checkButton = (Button)rootView.findViewById(R.id.check_availability);
        dateBtn = (ImageButton)rootView.findViewById(R.id.date_btn);
        timeBtn = (ImageButton)rootView.findViewById(R.id.time_btn);
        servicetype = (Spinner)rootView.findViewById(R.id.spinner_booking_servicetype) ;
        locationcombo = (Spinner)rootView.findViewById(R.id.spinner_booking_location_area) ;

        dateBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                android.support.v4.app.DialogFragment newFragment = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        String string_month;
                        String string_day;

                        if(month < 9){
                            string_month = "0"+(month+1);
                        }else{
                            string_month = Integer.toString(month + 1);
                        }
                        if(day < 10){
                            string_day = "0"+Integer.toString(day);
                        }else{
                            string_day = Integer.toString(day);
                        }
                    }
                };
                newFragment.show(getChildFragmentManager(), "datePicker");
            }
        });

        timeBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getFragmentManager(), "timePicker");
            }});

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("area", locationcombo.getSelectedItem().toString());
                editor.putString("service", servicetype.getSelectedItem().toString());
                editor.commit();

                android.support.v4.app.Fragment fragment = new FragmentBookingList();
                android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
            }
        });

        return rootView;
    }

}
