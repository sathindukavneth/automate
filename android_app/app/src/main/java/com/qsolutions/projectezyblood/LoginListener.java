package com.qsolutions.projectezyblood;

/**
 * Created by Choxmi on 1/15/2016.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import com.qsolutions.projectezyblood.login.ActivityBuilder;
import com.qsolutions.projectezyblood.login.ForgotPasswordActivity;
import com.qsolutions.projectezyblood.login.SignUpListener;
import com.qsolutions.projectezyblood.login.SkipActivity;
import com.squareup.otto.Bus;

public class LoginListener extends Activity implements GoogleApiClient.OnConnectionFailedListener{

    private static final int LOGIN_REQUEST = 0;
    private static final int REQUEST_LOCATION = 2;

    private ParseUser currentUser;
    private Button signinButton;
    private Button signupButton;
    private TextView skipLoginText;
    private TextView forgotPasswordText;
    private ImageView facebookLoginButton;
    private ImageView gPlusLoginButton;
    private GoogleApiClient mGoogleApiClient;
    private EditText userName;
    private EditText password;
    private Bundle parameters = new Bundle();
    private String mainuser;
    private String mainusername;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_layout);
        signinButton = (Button) findViewById(R.id.button_login_signin);
        signupButton = (Button) findViewById(R.id.button_login_signup);
        skipLoginText = (TextView) findViewById(R.id.text_login_skip);
        facebookLoginButton = (ImageView) findViewById(R.id.image_login_facebook);
        gPlusLoginButton = (ImageView) findViewById(R.id.image_login_google);
        userName = (EditText) findViewById(R.id.input_login_user);
        password = (EditText) findViewById(R.id.input_login_password);
        forgotPasswordText = (TextView) findViewById(R.id.text_login_forgot_password);

        mDialog = new ProgressDialog(LoginListener.this);
        mDialog.setMessage("Loading...");
        mDialog.setCancelable(false);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
//              .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(AppIndex.API).build();

        signinButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Name", userName.getText().toString());
                final ProgressDialog mDialog = new ProgressDialog(LoginListener.this);

                if (checkNetworkState()) {
                    mDialog.show();
                    if (userName.getText().toString() != "" && password.getText().toString() != "") {

                        currentUser.logInInBackground(userName.getText().toString().toLowerCase(), password.getText().toString(), new LogInCallback() {
                            @Override
                            public void done(ParseUser user, ParseException e) {
                                if (user != null) {
                                    currentUser = user;
                                    mainuser = currentUser.getUsername();
                                    mainusername = currentUser.get("name").toString();
                                    directToMainActivity();
                                    Toast.makeText(LoginListener.this, "Logged In", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(LoginListener.this, "Signup Before Login", Toast.LENGTH_SHORT).show();
                                    mDialog.hide();
                                }
                            }
                        });
                    } else {
                        Toast.makeText(LoginListener.this, "Enter Username and Password", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginListener.this, "Check Your Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        signupButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNetworkState()) {
                    if (currentUser != null) {
                        ParseUser.logOut();
                        currentUser = null;
                        directToSignUp();
                    } else {
                        directToSignUp();
                    }
                } else {
                    Toast.makeText(LoginListener.this, "Check Your Connection", Toast.LENGTH_SHORT);
                }
            }
        });

        skipLoginText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityBuilder loginBuilder = new ActivityBuilder(LoginListener.this, SkipActivity.class);
                startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
            }
        });

        facebookLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNetworkState()) {
                    mDialog.show();
                    final List<String> permissions = Arrays.asList("public_profile, email");
                    ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginListener.this, permissions, new LogInCallback() {
                        @Override
                        public void done(ParseUser user, ParseException err) {
                            if (user == null) {
                                mDialog.hide();
                                Toast.makeText(LoginListener.this, "Signin Cancelled", Toast.LENGTH_SHORT).show();
                            } else if (user.isNew()) {
                                currentUser = user;
                                facebookLogin();
                                Toast.makeText(LoginListener.this, "Created a Facebook User", Toast.LENGTH_SHORT).show();
                            } else {
                                currentUser = user;
                                facebookLogin();
                                Toast.makeText(LoginListener.this, "Already Signed Up Facebook User", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(LoginListener.this, "Check Your Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        gPlusLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.show();
                if (checkNetworkState()) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, 9001);
                } else {
                    mDialog.dismiss();
                    Toast.makeText(LoginListener.this, "Check Your Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        forgotPasswordText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNetworkState()) {
                    ActivityBuilder loginBuilder = new ActivityBuilder(LoginListener.this, ForgotPasswordActivity.class);
                    startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
                } else {
                    Toast.makeText(LoginListener.this, "Check Your Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        currentUser = ParseUser.getCurrentUser();

        if (currentUser != null) {
            Log.e("User is : ",currentUser.toString());
            directToMainActivity();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "LoginListener Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.qsolutions.projectezyblood/http/host/path")
        );
        AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "LoginListener Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.qsolutions.projectezyblood/http/host/path")
        );
        AppIndex.AppIndexApi.end(mGoogleApiClient, viewAction);
        mGoogleApiClient.disconnect();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 9001) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("Result", result.getStatus().toString());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String emailTest = acct.getEmail();
            mainusername = acct.getDisplayName() + " (Google)";
            Log.e("token", acct.getDisplayName());
            mainuser = emailTest;
            final String myname = acct.getDisplayName();

            ParseQuery<ParseObject> query = ParseQuery.getQuery("GoogleUsers");
            query.whereEqualTo("Email", mainuser);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> userList, ParseException e) {
                    if (e == null) {
                        Log.e("Size",String.valueOf(userList.size()));
                        if(userList.size() == 0){
                            Log.e("Size", String.valueOf(userList.size()));
                            currentUser = new ParseUser();
                            currentUser.setUsername(mainuser);
                            currentUser.setPassword("asd");
                            currentUser.setEmail(mainuser);
                            currentUser.put("name",mainusername);
                            currentUser.signUpInBackground(new SignUpCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        ParseUser.logInInBackground(mainuser, "asd", new LogInCallback() {
                                            @Override
                                            public void done(ParseUser user, ParseException e) {
                                                mDialog.dismiss();
                                                currentUser = user;
                                                directToMainActivity();
                                            }
                                        });
                                    } else {
                                        mDialog.dismiss();
                                        Log.e("Error", e.toString());
                                        Toast.makeText(LoginListener.this,"Error Occured",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            ParseObject googleObject = new ParseObject("GoogleUsers");
                            googleObject.put("Name", myname);
                            googleObject.put("Email", mainuser);
                            googleObject.saveInBackground();
                        }else{
                            Log.e("User", "Already Exist");
                            ParseUser.logInInBackground(mainuser, "asd", new LogInCallback() {
                                @Override
                                public void done(ParseUser user, ParseException e) {
                                    currentUser = user;
                                    directToMainActivity();
                                }
                            });
                        }

                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });

        } else {
            Toast.makeText(LoginListener.this,"Unsuccessful",Toast.LENGTH_SHORT).show();
        }
    }

    private void directToMainActivity() {
        if (currentUser != null) {
            Log.e("UserID",currentUser.getObjectId());
            Log.e("User", currentUser.toString());
            mDialog.hide();
            ActivityBuilder loginBuilder = new ActivityBuilder(
                    LoginListener.this, MainActivity.class, currentUser);
            startActivityForResult(loginBuilder.mainbuild(), LOGIN_REQUEST);
        }
    }

    private void directToSignUp(){
        Toast.makeText(LoginListener.this,"Sign Up",Toast.LENGTH_SHORT).show();
        ActivityBuilder loginBuilder = new ActivityBuilder(
                LoginListener.this,SignUpListener.class);
        startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
        finish();
    }

    public boolean checkNetworkState(){
        ConnectivityManager connec = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connec != null && (
                (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) ||
                        (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED))) {

            return true;

        } else if (connec != null && (
                (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) ||
                        (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED ))) {
            Toast.makeText(getApplicationContext(), "You must be connected to the internet", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    private void facebookLogin(){
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override

                    public void onCompleted(
                            final JSONObject object,
                            GraphResponse response) {
                        try {
                            mainusername = object.getString("name");
                            mainuser = mainusername + " (facebook)";
                            final String username = currentUser.getUsername();
                            final ParseQuery<ParseObject> query = ParseQuery.getQuery("FacebookUsers");
                            query.whereEqualTo("UserName",username);
                            query.findInBackground(new FindCallback<ParseObject>() {
                                public void done(List<ParseObject> userList, ParseException e) {
                                    if (e == null) {
                                        if (userList.size() == 0) {
                                            Log.e("Object", mainusername);
                                            ParseObject fbObject = new ParseObject("FacebookUsers");
                                            fbObject.put("Name", mainusername);
                                            fbObject.put("Email", mainuser);
                                            fbObject.put("UserName", username);
                                            fbObject.saveInBackground();
                                            directToMainActivity();
                                        } else {
                                            directToMainActivity();
                                        }
                                    } else {
                                        Log.d("score", "Error: " + e.getMessage());
                                    }
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        parameters.putString("fields", "id,name,link");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void onBackPressed() {
        new AlertDialog.Builder(this)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            moveTaskToBack(true);
                        }
                    }).create().show();

    }

}
