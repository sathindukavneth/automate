package com.qsolutions.projectezyblood.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.qsolutions.projectezyblood.MainActivity;
import com.qsolutions.projectezyblood.R;
import com.qsolutions.projectezyblood.adapters.ExpandableListAdapter;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by Sathindu on 2015-12-30.
 */
public class FragmentNotifications extends Fragment{

    View rootView;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, String> listDataChild;
    String listRowData;

    public FragmentNotifications(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        prepareListData();

        rootView = inflater.inflate(R.layout.fragment_notifications, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);

        // preparing list data


        listAdapter = new ExpandableListAdapter(this.getActivity(), listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);


    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();

        listDataChild = new HashMap<String, String>();


        final ParseQuery<ParseObject> queryProfile = ParseQuery.getQuery("UserProfile");
        queryProfile.whereEqualTo("email", ((MainActivity) getActivity()).getEmail());
        queryProfile.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e==null){
                    if (objects.size()==0){
                        new AlertDialog.Builder(getActivity())
                                .setTitle("No Profile Found")
                                .setMessage("Create your profile to get Notifications")
                                .setPositiveButton("Create", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Intent profile = new Intent(getActivity(),FragmentProfile.class);
                                        startActivity(profile);
                                    }
                                })
                                .setNegativeButton(android.R.string.cancel, null).show();
                    }
                    else{
                        ParseQuery<ParseObject> query = ParseQuery.getQuery("BloodRequest");
                        query.whereMatchesKeyInQuery("bloodtype","bloodgroup",queryProfile);
                        Log.e("Query", "Checking the query");
                        query.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> objects, ParseException e) {
                                Log.e("Query", "Done");
                                if (objects != null) {
                                    for (int i = 0; i < objects.size(); i++) {
                                        Log.e("Loop Check", objects.get(i).get("email").toString());
                                        listDataHeader.add(objects.get(i).get("email").toString());
                                        listRowData = ("Blood Group : " + objects.get(i).get("bloodtype").toString() + " \nMobile : " + objects.get(i).get("mobile").toString() + " \nMessage : " + objects.get(i).get("message").toString());
                                        listDataChild.put(listDataHeader.get(i), listRowData);
                                        listRowData = "";
                                    }
                                }

                                listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);

                                // setting list adapter
                                expListView.setAdapter(listAdapter);
                            }
                        });
                    }
                }
            }
        });
    }
}
