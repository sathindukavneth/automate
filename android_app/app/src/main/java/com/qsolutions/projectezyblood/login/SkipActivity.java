package com.qsolutions.projectezyblood.login;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.qsolutions.projectezyblood.R;
import com.qsolutions.projectezyblood.fragments.FragmentSkipRequestBlood;

public class SkipActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skip);

        Fragment fragment = null;
        getSupportActionBar().setTitle("Request Blood");
        fragment = new FragmentSkipRequestBlood();

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.skip_frame_container, fragment).commit();
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }


    }

}
