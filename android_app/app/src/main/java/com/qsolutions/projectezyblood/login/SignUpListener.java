package com.qsolutions.projectezyblood.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import com.qsolutions.projectezyblood.LoginListener;
import com.qsolutions.projectezyblood.R;

/**
 * Created by Choxmi on 1/14/2016.
 */
public class SignUpListener extends AppCompatActivity {

    Button signUpButton;
    Button cancelButton;
    EditText emailEdit;
    EditText userNameEdit;
    EditText passwordEdit;
    EditText passwordRetype;
    ParseUser EZYUser = new ParseUser();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);

        signUpButton = (Button)findViewById(R.id.button_signup_signup);
        cancelButton = (Button) findViewById(R.id.button_signup_cancel);
        emailEdit = (EditText)findViewById(R.id.input_signup_email);
        userNameEdit = (EditText)findViewById(R.id.input_signup_name);
        passwordEdit = (EditText)findViewById(R.id.input_signup_password);
        passwordRetype = (EditText)findViewById(R.id.input_signup_retypepassword);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordEdit.getText().toString().equals(passwordRetype.getText().toString()) && userNameEdit.getText().toString()!=""&&emailEdit.getText().toString()!="") {
                    EZYUser.setUsername(emailEdit.getText().toString().toLowerCase());
                    EZYUser.setEmail(emailEdit.getText().toString().toLowerCase());
                    EZYUser.setPassword(passwordEdit.getText().toString());
                    EZYUser.put("name", userNameEdit.getText().toString());
                    final ProgressDialog signupwait = new ProgressDialog(SignUpListener.this);
                    signupwait.setMessage("Signing up...");
                    signupwait.show();
                    EZYUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                signupwait.hide();
                                Toast.makeText(SignUpListener.this, "Sign Up successful", Toast.LENGTH_SHORT).show();
                                Log.e("SignUp", "successful");

                                directToLoginActivity();
                            } else {
                                signupwait.hide();
                                Toast.makeText(SignUpListener.this, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(getBaseContext(), "Details does not match", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(),"Cancel",Toast.LENGTH_SHORT).show();
                ActivityBuilder loginBuilder = new ActivityBuilder(
                        SignUpListener.this,LoginListener.class);
                startActivityForResult(loginBuilder.build(), 1);
                finish();
            }
        });

    }

    private void directToLoginActivity(){
        finish();
        ActivityBuilder loginBuilder = new ActivityBuilder(
                SignUpListener.this, LoginListener.class);
        startActivityForResult(loginBuilder.build(), 1);
    }
}
