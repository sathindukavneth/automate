package com.qsolutions.projectezyblood.fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.qsolutions.projectezyblood.HttpHandler;
import com.qsolutions.projectezyblood.Location.GPSTracker;
import com.qsolutions.projectezyblood.Locations;
import com.qsolutions.projectezyblood.MainActivity;
import com.qsolutions.projectezyblood.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Choxmi on 2016-10-13.
 */

public class FragmentGasStation extends android.support.v4.app.Fragment implements OnMapReadyCallback {

    private GoogleMap googleMap;

    double latitude = 6.9344;
    double longtitude = 79.8428;

    ProgressDialog progressDialog;
    ArrayList<Locations> locationList=null;

    ListView listView;


    String url;
    String current_Name;
    String current_details;

    public FragmentGasStation() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_gasstation, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map_request_location);
        mapFragment.getMapAsync(this);

        locationList = new ArrayList<Locations>();

        url = "http://10.0.2.2:8080/WebServiceRest/webresources/webService/colombo&-and-type=&Fuel-Station&";

        listView = (ListView) rootView.findViewById(R.id.gas_lv);

        //new GetDetails().execute();

        return rootView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMyLocationEnabled(true);
        setManual();
        setCurrentLocation();
        manualSetMap();

        //code to set latitude longitude of current location when press location button in map
        this.googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                setCurrentLocation();
                manualSetMap();
                return false;
            }
        });
    }

    private void setCurrentLocation() {
        GPSTracker gps = new GPSTracker(getActivity());
        if(gps.canGetLocation()){
            latitude = gps.getLatitude();
            longtitude = gps.getLongitude();
            Toast.makeText(getActivity(), "Location Set to Your Current Location", Toast.LENGTH_SHORT).show();

            for (int j=0;j<locationList.size();j++) {
                LatLng mylocation = new LatLng(locationList.get(j).getLat(),locationList.get(j).getLng());
                googleMap.addMarker(new MarkerOptions().position(mylocation).title(locationList.get(j).getStore()));
                Log.e("Location","Added");
            }

        }else{
            gps.showSettingsAlert();
        }
    }

    public void setManual(){

        LatLng mylocation = new LatLng(6.911484,79.877249);
        googleMap.addMarker(new MarkerOptions().position(mylocation).title("Staford motors "));
        LatLng mylocation1 = new LatLng(6.918609,79.874043);
        googleMap.addMarker(new MarkerOptions().position(mylocation1).title("Mag City"));
        LatLng mylocation2 = new LatLng(6.923698,79.861796);
        googleMap.addMarker(new MarkerOptions().position(mylocation2).title("KIA Motors Lanka (Head Office)"));
        LatLng mylocation3 = new LatLng(6.92038,79.858154);
        googleMap.addMarker(new MarkerOptions().position(mylocation3).title("Lanka IOC Fuel Station"));

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                new AlertDialog.Builder(getContext())
                        .setTitle(marker.getTitle())
                        .setMessage("Dudley Senanayake Mawatha, Colombo 00700 \n 118456742")
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                return false;
            }
        });
    }

    public void manualSetMap() {
        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longtitude)).title("Lanka IOC Fuel Station");
        Log.e("MAP SET", longtitude + "/" + latitude);

        this.googleMap.clear();
        this.googleMap.addMarker(marker);
        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longtitude), 15);
        this.googleMap.animateCamera(update);
    }



    private class GetDetails extends AsyncTask<Void,Void,Void> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog= new ProgressDialog(getContext());
                progressDialog.setMessage("Retreiveing data...");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                HttpHandler httpHandler = new HttpHandler();

                JSONArray jsonStr = httpHandler.makeServiceCall(url);
                Log.e("Response","Response is "+jsonStr);
                for(int i=0;i<jsonStr.length();i++){
                    try {

                        JSONObject obj = jsonStr.getJSONObject(i);

                        Locations locations = new Locations();
                        locations.setStore(obj.getString("name"));
                        locations.setLat(obj.getDouble("latitude"));
                        locations.setLng(obj.getDouble("longtitude"));
                        locations.setAddress(obj.getString("address"));
                        locations.setType(obj.getString("type"));
                        locations.setTel(obj.getInt("tel"));
                        Log.e("Latitude",String.valueOf(locations.getLat()));
                        Log.e("Longtitude",String.valueOf(locations.getLng()));
                        locationList.add(locations);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                for (int j=0;j<locationList.size();j++) {
                    LatLng mylocation = new LatLng(locationList.get(j).getLat(),locationList.get(j).getLng());
                    googleMap.addMarker(new MarkerOptions().position(mylocation).title(locationList.get(j).getStore()));
                    current_Name =locationList.get(j).getStore();
                    current_details = locationList.get(j).getAddress()+"\n"+locationList.get(j).getType()+"\n"+locationList.get(j).getTel();
                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(marker.getTitle())
                                    .setMessage(current_details)
//                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();

                            return false;
                        }
                    });
                    Log.e("Location","Added");
                }
            }
    }
}



